import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {FetchDataService} from '../fetch-data.service';
import { $ } from '../../../node_modules/protractor';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    user: any;
    password: '';
    param = {};
    Response: number;

  constructor( public route: Router, public fetchdataService: FetchDataService) {

   }

  ngOnInit() {
  }
  onSubmit() {
    const url = environment.urls.login;
    this.param = `username= ${this.user}&password= ${this.password}`;
    this.fetchdataService.postData(url, this.param).subscribe(
      res => {
        console.log(res);
        // this.Response = res.status.code;
        console.log(this.Response);
        if (this.Response === 1) {
           this.route.navigate(['/main']);
        }
      },
      err => {
        console.log('Error occured');
      }
    );

  }

}
