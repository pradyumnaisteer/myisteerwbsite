import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchDataService {

  // loader: ILoader= {isLoading: false};
  constructor(public http: HttpClient) {}
 /* Encode the Form Data*/
  getFormUrlEncoded(toConvert) {
    const formBody = [];
    for (const property in toConvert) {
      if (toConvert.hasOwnProperty(property)) {
        const encodedKey = encodeURIComponent(property);
        const encodedValue = JSON.parse(toConvert[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
    }
    return formBody.join('&');
  }

  /* Code Ends Here */

  /** Post Calls */
 // Observable<any>
  postData(url, data) {
     const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    //  const _data = this.getFormUrlEncoded(data);
     return this.http.post(url, data, httpOptions);
  }

  /** Get Calls */
  // getData(url, data, headers): Observable<any> {
  //   return this.http.get(environment.urls[url], {headers: headers, params: data});
  //   }

  /** Ends Here */

    /** Loader Code */
  // showLoader() {
  //   console.log('showloader started');
  //   this.loader.isLoading = true;
  // }
  // hideLoader() {
  //   this.loader.isLoading = false;
  // }
}
