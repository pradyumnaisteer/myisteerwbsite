import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {LoginComponent} from './login/login.component';
import { MainComponent } from '../app/login/main/main.component';

const routes: Routes = [
{path: '', redirectTo: 'login', pathMatch: 'full'  },
{path: 'login', component: LoginComponent},
{path: 'main', component: MainComponent}

];
@NgModule({
  imports: [RouterModule.forRoot(routes), RouterTestingModule],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingComponents = [
  ];
